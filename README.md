## Welcome to the JIRA Spring Boot Client project

This project tries to provide a basic JIRA REST API Client based on common, modern Spring practices.
In particular it provides a default [Spring Boot Autoconfiguration](http://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-auto-configuration.html), 
but it can be used without Spring Boot as well.

Technologies used:

* Spring Boot auto configuration
* [RestTemplate](http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/client/RestTemplate.html) as HttpClient abstraction
* [Jackson](https://github.com/FasterXML/jackson) for serializing and deserializing
* [Hibernate Validator](http://hibernate.org/validator/) for bean validation

Additional tools:

* [Lombok](https://projectlombok.org)
* [Spock](http://spockframework.org) for testing

### Quick start for Spring Boot projects

Add the starter module as Maven dependency:

    <dependency>
        <groupId>com.pep1.jira</groupId>
        <artifactId>jira-spring-boot-client-starter</artifactId>
        <version>${pep1-jira-client.version}</version>
    </dependency>

Adjust the configuration in your ``src/main/resources/application.yml``

    jira:
      enabled:      true
      instance:
        username:   ${YourJIRAUsername}
        password:   ${YourJIRAPassword}
        uri:        ${YourJIRAURL}      # e.g. https://yourname.atlassian.net

And create your new your new Spring Boot application:

    @SpringBootApplication
    public class MyJIRAApplication implements CommandLineRunner {
    
        private static final Logger log = LoggerFactory.getLogger(MyJIRAApplication.class);
    
        public static void main(String[] args) {
            SpringApplication.run(MyJIRAApplication.class, args);
        }
    
        @Autowired
        private JIRAClient jiraClient;
    
        @Override
        public void run(String... args) throws Exception {
            for (String issueKey : args) {
                log.info("Loading JIRA issue with key '{}'", issueKey);
                log.info("Response: {}", jiraClient.getIssue(issueKey));
            }
        }
    }

For a working example please have a look at the sample application in the ``example`` sub module.

### Customization

For fine grained customization have a look at the ``JIRAClientConfig`` class.
It's designed to be adjustable to more specific needs. If your project for example requires a custom
Jackson ``Objectmapper``, you can override the default one by specifying your own named Bean:

    @Bean(JIRA_OBJECT_MAPPER_BEAN_NAME)
    public ObjectMapper jiraObjectMapper() {
        return new Jackson2ObjectMapperBuilder()
                .failOnEmptyBeans(false)
                .failOnUnknownProperties(false)
                .dateFormat(new ISO8601DateFormat())
                .timeZone(TimeZone.getTimeZone("UTC"))
                .modulesToInstall(new JodaModule())
                .build();
    }

However the default beans defined in the ``JIRAClientConfig`` class should give you a working, ready to go starting 
point.

### Caching

If you want to enable result caching for received JIRA resources, just add the caching auto configuration:

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-cache</artifactId>
    </dependency>

### Todo list

- [ ] multi instance support
- [x] caching support
- [x] input validation
- [ ] complete testing
- [ ] complete REST Api coverage of the [JIRA REST API](https://docs.atlassian.com/jira/REST/cloud/)
