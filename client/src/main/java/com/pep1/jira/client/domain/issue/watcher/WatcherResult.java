package com.pep1.jira.client.domain.issue.watcher;

import com.pep1.jira.client.domain.DTO;
import com.pep1.jira.client.domain.user.User;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class WatcherResult implements DTO {

    private String self;
    private Boolean isWatching;
    private Integer watchCount;
    private List<User> watchers = new ArrayList<User>();
}
