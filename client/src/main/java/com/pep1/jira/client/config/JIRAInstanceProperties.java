package com.pep1.jira.client.config;

import lombok.Data;

import java.net.URI;
import java.net.URISyntaxException;

@Data
public class JIRAInstanceProperties {

    /**
     * The username to authenticate.
     */
    private String username = "admin";

    /**
     * The password to authenticate
     */
    private String password = "admin";

    /**
     * The JIRA uri including the sub path (e.g. http://localhost:8080/jira)
     */
    private URI uri;

    /**
     * The Client connection timeout.
     */
    private Integer connectionTimeout = 5000;

    /**
     * The Client read timeout.
     */
    private Integer readTimeout = 5000;

    public JIRAInstanceProperties() {
        try {
            // workaround to set default value with catching syntax exception
            uri = new URI("http://localhost:8080");
        } catch (URISyntaxException e) {
            // this should not happen
        }
    }
}
