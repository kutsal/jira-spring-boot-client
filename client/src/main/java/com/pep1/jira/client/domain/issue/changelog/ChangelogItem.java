package com.pep1.jira.client.domain.issue.changelog;

import com.pep1.jira.client.domain.DTO;
import lombok.Data;

@Data
public class ChangelogItem implements DTO {
    private String field;
    private String fieldtype;
    private String from;
    private String fromString;
    private String to;
    private String toString;
}
