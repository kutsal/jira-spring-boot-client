package com.pep1.jira.client.domain.user.request;

import com.pep1.jira.client.domain.DTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserSearch implements DTO{
    /**
     * Can be username, name or email address
     */
    private String username;
    private Integer startAt;
    private Integer maxResults;
    private Boolean includeActive;
    private Boolean includeInactive;
    private String property;
}
