package com.pep1.jira.client.domain.webhook.event;

import com.atlassian.connect.spring.AtlassianHost;
import com.pep1.jira.client.domain.DTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InstanceDTO implements DTO {
    private String id;
    private String baseUrl;
    private String productType;
    private String description;

    public static InstanceDTO fromHost(@NonNull AtlassianHost atlassianHost) {
        return InstanceDTO.builder()
                .baseUrl(atlassianHost.getBaseUrl())
                .id(atlassianHost.getClientKey())
                .description(atlassianHost.getDescription())
                .productType(atlassianHost.getProductType())
                .build();
    }
}
