package com.pep1.jira.client.domain.issue.changelog;

import com.pep1.jira.client.domain.DTO;
import lombok.Data;

import java.util.List;

@Data
public class Changelog implements DTO{
    private String id;
    private List<ChangelogItem> items;
}
