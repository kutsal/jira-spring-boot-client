package com.pep1.jira.client.domain.field;

import com.pep1.jira.client.domain.DTO;
import lombok.Data;

@Data
public class Field implements DTO {
    private String id;
    private String name;
    private boolean custom;
    private boolean orderable;
    private boolean navigable;
    private boolean searchable;
    private String[] clauseNames;
    private Schema schema;
}
