package com.pep1.jira.client.domain.field;

import com.pep1.jira.client.domain.DTO;
import lombok.Data;

@Data
public class Schema implements DTO {
    private String type;
    private String items;
    private String system;
    private String custom;
    private int customId;
}
